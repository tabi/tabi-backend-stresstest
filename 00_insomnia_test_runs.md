# Insomnia test runs

- Install insomnia: `sudo snap install insomnia`
- Import swagger file
- Use Swagger env
- Edit Swagger env:
  ```
  {
	"base_path": "/api/v1",
	"did": "1",
	"host": "tabi.cbs.nl",
	"scheme": "https",
	"uid": "2"
  }
  ```

## Get a Bearer token for a user

JSON
```
{
	"Password": "your_password",
	"Username": "your_username"
}
```
CURL
```
curl --request POST \
  --url https://tabi.cbs.nl/api/v1/token \
  --header 'content-type: application/json' \
  --data '{
	"Password": "your_password",
	"Username": "your_username"
}'
```
### RESPONSE

`200 OK`

RESULT

```
{
	"UserId": 2,
	"Token": "your_token"
}
```

HEADER

```
Content-Type: application/json
Date: Sun, 07 Oct 2018 14:37:09 GMT
Content-Length: 204
Strict-Transport-Security: max-age=31536000; includeSubDomains
```

<details><summary>TIMELINE</summary>

```
* Preparing request to https://tabi.cbs.nl/api/v1/token
* Using libcurl/7.59.0 OpenSSL/1.1.0h zlib/1.2.11 nghttp2/1.31.1
* Enable automatic URL encoding
* Enable SSL validation
* Enable cookie sending with jar of 0 cookies
*   Trying 87.213.43.243...
* TCP_NODELAY set
* Connected to tabi.cbs.nl (87.213.43.243) port 443 (#2)
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*   CAfile: /run/user/1000/snap.insomnia/insomnia_6.0.2/2017-09-20.pem
*   CApath: none
* TLSv1.2 (OUT), TLS handshake, Client hello (1):
* TLSv1.2 (IN), TLS handshake, Server hello (2):
* TLSv1.2 (IN), TLS handshake, Certificate (11):
* TLSv1.2 (IN), TLS handshake, Server key exchange (12):
* TLSv1.2 (IN), TLS handshake, Server finished (14):
* TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
* TLSv1.2 (OUT), TLS change cipher, Client hello (1):
* TLSv1.2 (OUT), TLS handshake, Finished (20):
* TLSv1.2 (IN), TLS handshake, Finished (20):
* SSL connection using TLSv1.2 / ECDHE-RSA-AES128-SHA256
* ALPN, server accepted to use http/1.1
* Server certificate:
*  subject: C=NL; ST=Zuid-Holland; L='s-Gravenhage; O=Centraal Bureau voor de Statistiek; OU=BTB; CN=*.cbs.nl
*  start date: Sep  6 12:11:01 2017 GMT
*  expire date: Sep  6 12:21:00 2020 GMT
*  subjectAltName: host "tabi.cbs.nl" matched cert's "*.cbs.nl"
*  issuer: C=BM; O=QuoVadis Limited; CN=QuoVadis Global SSL ICA G2
*  SSL certificate verify ok.
> POST /api/v1/token HTTP/1.1
> Host: tabi.cbs.nl
> User-Agent: insomnia/6.0.2
> Content-Type: application/json
> Accept: */*
> Content-Length: 44
| {
| 	"Password": "your_password",
| 	"Username": "your_username"
| }
* upload completely sent off: 44 out of 44 bytes
< HTTP/1.1 200 OK
< Content-Type: application/json
< Date: Sun, 07 Oct 2018 14:37:09 GMT
< Content-Length: 204
< Strict-Transport-Security: max-age=31536000; includeSubDomains

* Received 204 B chunk
* Connection #2 to host tabi.cbs.nl left intact
```
</details>

## List all devices beloning to a user

BEARER

```
Token: your_token
```

CURL

```
curl --request GET \
  --url https://tabi.cbs.nl/api/v1/user/2/device \
  --header 'authorization: Bearer your_token'
```

### RESPONSE

`200 OK`

RESULT

```
[
	{
		"ID": 1,
		"CreatedAt": "2018-04-30T16:15:29.566349+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "7.0",
		"OperatingSystemVersion": null
	},
	{
		"ID": 2,
		"CreatedAt": "2018-04-30T16:36:27.758555+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "7.0",
		"OperatingSystemVersion": null
	},
	{
		"ID": 3,
		"CreatedAt": "2018-04-30T16:41:18.610473+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "7.0",
		"OperatingSystemVersion": null
	},
	{
		"ID": 4,
		"CreatedAt": "2018-04-30T16:45:48.77828+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "7.0",
		"OperatingSystemVersion": null
	},
	{
		"ID": 5,
		"CreatedAt": "2018-04-30T16:54:26.197861+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "7.0",
		"OperatingSystemVersion": null
	},
	{
		"ID": 6,
		"CreatedAt": "2018-04-30T17:01:06.448314+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "7.0",
		"OperatingSystemVersion": null
	},
	{
		"ID": 7,
		"CreatedAt": "2018-04-30T17:09:31.893222+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "7.0",
		"OperatingSystemVersion": null
	},
	{
		"ID": 8,
		"CreatedAt": "2018-04-30T17:16:16.699377+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "7.0",
		"OperatingSystemVersion": null
	},
	{
		"ID": 9,
		"CreatedAt": "2018-04-30T17:24:12.526437+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "7.0",
		"OperatingSystemVersion": null
	},
	{
		"ID": 10,
		"CreatedAt": "2018-04-30T17:31:30.947398+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "7.0",
		"OperatingSystemVersion": null
	},
	{
		"ID": 11,
		"CreatedAt": "2018-04-30T17:42:27.238868+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "7.0",
		"OperatingSystemVersion": null
	},
	{
		"ID": 12,
		"CreatedAt": "2018-04-30T18:01:08.820983+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "7.0",
		"OperatingSystemVersion": null
	},
	{
		"ID": 17,
		"CreatedAt": "2018-05-09T16:07:12.050198+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "7.0",
		"OperatingSystemVersion": null
	},
	{
		"ID": 21,
		"CreatedAt": "2018-05-09T18:04:20.928796+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "Android",
		"OperatingSystemVersion": "7.0"
	},
	{
		"ID": 144,
		"CreatedAt": "2018-07-12T13:25:19.849741+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "Android",
		"OperatingSystemVersion": "7.0"
	},
	{
		"ID": 146,
		"CreatedAt": "2018-07-12T14:32:38.995197+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "Android",
		"OperatingSystemVersion": "7.0"
	},
	{
		"ID": 211,
		"CreatedAt": "2018-09-04T14:04:33.733899+02:00",
		"UpdatedAt": null,
		"DeletedAt": null,
		"UserId": 2,
		"Manufacturer": "LGE",
		"Model": "LG-H870",
		"OperatingSystem": "Android",
		"OperatingSystemVersion": "7.0"
	}
]
```
HEADER

```
Content-Type: application/json
Date: Sun, 07 Oct 2018 14:37:09 GMT
Content-Length: 204
Strict-Transport-Security: max-age=31536000; includeSubDomains
```

<details><summary>TIMELINE</summary>

```
* Preparing request to https://tabi.cbs.nl/api/v1/token
* Using libcurl/7.59.0 OpenSSL/1.1.0h zlib/1.2.11 nghttp2/1.31.1
* Enable automatic URL encoding
* Enable SSL validation
* Enable cookie sending with jar of 0 cookies
*   Trying 87.213.43.243...
* TCP_NODELAY set
* Connected to tabi.cbs.nl (87.213.43.243) port 443 (#2)
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*   CAfile: /run/user/1000/snap.insomnia/insomnia_6.0.2/2017-09-20.pem
*   CApath: none
* TLSv1.2 (OUT), TLS handshake, Client hello (1):
* TLSv1.2 (IN), TLS handshake, Server hello (2):
* TLSv1.2 (IN), TLS handshake, Certificate (11):
* TLSv1.2 (IN), TLS handshake, Server key exchange (12):
* TLSv1.2 (IN), TLS handshake, Server finished (14):
* TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
* TLSv1.2 (OUT), TLS change cipher, Client hello (1):
* TLSv1.2 (OUT), TLS handshake, Finished (20):
* TLSv1.2 (IN), TLS handshake, Finished (20):
* SSL connection using TLSv1.2 / ECDHE-RSA-AES128-SHA256
* ALPN, server accepted to use http/1.1
* Server certificate:
*  subject: C=NL; ST=Zuid-Holland; L='s-Gravenhage; O=Centraal Bureau voor de Statistiek; OU=BTB; CN=*.cbs.nl
*  start date: Sep  6 12:11:01 2017 GMT
*  expire date: Sep  6 12:21:00 2020 GMT
*  subjectAltName: host "tabi.cbs.nl" matched cert's "*.cbs.nl"
*  issuer: C=BM; O=QuoVadis Limited; CN=QuoVadis Global SSL ICA G2
*  SSL certificate verify ok.
> POST /api/v1/token HTTP/1.1
> Host: tabi.cbs.nl
> User-Agent: insomnia/6.0.2
> Content-Type: application/json
> Accept: */*
> Content-Length: 44
| {
| 	"Password": "your_password",
| 	"Username": "your_username"
| }
* upload completely sent off: 44 out of 44 bytes
< HTTP/1.1 200 OK
< Content-Type: application/json
< Date: Sun, 07 Oct 2018 14:37:09 GMT
< Content-Length: 204
< Strict-Transport-Security: max-age=31536000; includeSubDomains

* Received 204 B chunk
* Connection #2 to host tabi.cbs.nl left intact
```
</details>

## Add positions to device

JSON
```
[
  {
    "Accuracy": 3.1415,
    "Altitude": 42,
    "CreatedAt": "2018-10-07T13:50:01.554Z",
    "DesiredAccuracy": 0,
    "DeviceID": 1,
    "DistanceBetweenPreviousPosition": 5,
    "Heading": 0,
    "ID": 0,
    "Latitude": 5,
    "Longitude": 5,
    "Speed": 5,
    "Timestamp": "2018-10-07T13:50:01.555Z"
  }
]
```

CURL

```
curl --request POST \
  --url https://tabi.cbs.nl/api/v1/user/2/device/1/positionentry \
  --header 'authorization: Bearer your_token' \
  --data '[
  {
    "Accuracy": 3.1415,
    "Altitude": 42,
    "CreatedAt": "2018-10-07T13:50:01.554Z",
    "DesiredAccuracy": 0,
    "DeviceID": 1,
    "DistanceBetweenPreviousPosition": 5,
    "Heading": 0,
    "ID": 0,
    "Latitude": 5,
    "Longitude": 5,
    "Speed": 5,
    "Timestamp": "2018-10-07T13:50:01.555Z"
  }
]'
```

BEARER

```
Token: your_token
```

### RESPONSE

`200 OK`

RESULT

```
[empty]
```

HEADER

```
Date: Sun, 07 Oct 2018 14:48:53 GMT
Content-Length: 0
Strict-Transport-Security: max-age=31536000; includeSubDomains
```

<details><summary>TIMELINE</summary>

```
* Preparing request to https://tabi.cbs.nl/api/v1/user/2/device/1/positionentry
* Using libcurl/7.59.0 OpenSSL/1.1.0h zlib/1.2.11 nghttp2/1.31.1
* Enable automatic URL encoding
* Enable SSL validation
* Enable cookie sending with jar of 0 cookies
* Found bundle for host tabi.cbs.nl: 0x1be3a247f210 [can pipeline]
* Re-using existing connection! (#4) with host tabi.cbs.nl
* Connected to tabi.cbs.nl (87.213.43.243) port 443 (#4)
> POST /api/v1/user/2/device/1/positionentry HTTP/1.1
> Host: tabi.cbs.nl
> User-Agent: insomnia/6.0.2
> Authorization: Bearer your_token
> Accept: */*
> Content-Length: 317
| [
|   {
|     "Accuracy": 3.1415,
|     "Altitude": 42,
|     "CreatedAt": "2018-10-07T13:50:01.554Z",
|     "DesiredAccuracy": 0,
|     "DeviceID": 1,
|     "DistanceBetweenPreviousPosition": 5,
|     "Heading": 0,
|     "ID": 0,
|     "Latitude": 5,
|     "Longitude": 5,
|     "Speed": 5,
|     "Timestamp": "2018-10-07T13:50:01.555Z"
|   }
| ]
* upload completely sent off: 317 out of 317 bytes
< HTTP/1.1 200 OK
< Date: Sun, 07 Oct 2018 14:48:53 GMT
< Content-Length: 0
< Strict-Transport-Security: max-age=31536000; includeSubDomains

* Connection #4 to host tabi.cbs.nl left intact
```

</details>
